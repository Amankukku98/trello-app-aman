import React from 'react';
import {
  Switch,
  Route,
  BrowserRouter,
} from "react-router-dom";
import Navbar from './Components/Navbar/Navbar';
import Board from './Components/Boards/Boards';
import ListDetails from './Components/ListDetails/ListDetails';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Navbar />
        <Switch>

          <Route exact path="/" component={Board} />
          <Route exact path="/:id" component={ListDetails} />

        </Switch>
      </BrowserRouter>

    )
  }
}

export default App;