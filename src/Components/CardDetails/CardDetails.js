import React, { Component } from 'react';
import * as TrelloApi from '../../api';

import SaveIcon from '@mui/icons-material/Save';
import DeleteIcon from '@mui/icons-material/Delete';
import LaptopMacOutlinedIcon from '@mui/icons-material/LaptopMacOutlined';
import RadioOutlinedIcon from '@mui/icons-material/RadioOutlined';

import AddIcon from '@mui/icons-material/Add';
import {
    Box, Text, Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverBody,
    PopoverArrow,
    PopoverCloseButton,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
} from '@chakra-ui/react';
import Cheklist from '../Checklist/Checklist';



class CardDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            hasError: false,
            cardName: '',
            isOpen: false,
        }
    }
    createCard = () => {
        TrelloApi.createCard(this.props.listId, this.state.cardName)
            .then((res) => {
                const newCards = [...this.state.cards];
                newCards.push(res);
                this.setState({ cards: newCards, cardName: '', isOpen: false, })

            })
    }
    handleDelete = (id) => {
        TrelloApi.deleteCard(id)
            .then((res) => {
                const newCards = this.state.cards.filter((card) => {
                    return card.id !== id;
                })
                this.setState({ cards: newCards });
            })
    }
    handleOpen = () => {
        this.setState({ isOpen: true });
    }
    handClose = () => {
        this.setState({ isOpen: false });
    }


    componentDidMount() {

        TrelloApi.getCard(this.props.listId)
            .then(card => {
                this.setState({
                    cards: card,
                    hasError: false,

                });
            }).catch((err) => {
                this.setState({
                    cards: [],
                    hasError: "Failed to load cards",
                });
            })

    }
    render() {
        const { cards } = this.state;
        return (
            <>
                {cards.map((card, index) => {
                    return <Box bg="white" mt={4} key={index} display="flex" justifyContent="space-between" p={2} borderRadius={20}>


                        <Popover placement="right" ml="15rem">
                            <PopoverTrigger>
                                <Text cursor="pointer">{card.name}</Text>

                            </PopoverTrigger>
                            <PopoverContent w="30rem" mt="7rem" ml="-4rem">
                                <PopoverArrow />
                                <PopoverBody fontSize={24} ml={0}><Box color="gray" ml={1} display="flex"><LaptopMacOutlinedIcon /><Text color="black" ml={2} mt={-2} fontWeight={700}>{card.name}</Text></Box>
                                </PopoverBody>
                                <PopoverBody ml="18rem" lineHeight={8}>
                                    <Text mt={5}>Actions</Text>
                                    <Button w={165} onClick={() => { this.handleDelete(card.id) }} ><Box mr={3} ml={-12}  ><RadioOutlinedIcon /></Box>Archive</Button>
                                </PopoverBody>
                                <PopoverBody>
                                    <Cheklist cardId={card.id} />
                                </PopoverBody>
                                <Box >
                                    <PopoverCloseButton fontSize={18} />
                                </Box>
                            </PopoverContent>
                        </Popover>

                        <Box mr={3} color="gray.600">  <DeleteIcon onClick={() => { this.handleDelete(card.id) }} cursor="pointer" /></Box>
                    </Box>

                })}
                <Box>
                    <Box display="flex" justifyContent="space-between">
                        <Button mt={6} ml={4} bg="gray.300" color="gray.50" border="none" onClick={this.handleOpen}> <AddIcon /> Add a card</Button> <Box mt={8} color="gray.50" mr={3}><SaveIcon /></Box>
                    </Box >
                    <Modal isOpen={this.state.isOpen}>
                        <ModalOverlay />
                        <ModalContent mt="20rem" mr="10rem">
                            <ModalHeader>Create a card</ModalHeader>
                            <ModalBody>
                                <Input placeholder='Enter a title for this card...' onChange={e => { this.setState({ cardName: e.target.value }) }} ></Input>
                            </ModalBody>
                            <ModalFooter>
                                <Button colorScheme='blue' mr={3} onClick={this.handClose}>
                                    Close
                                </Button>
                                <Button variant='ghost' onClick={this.createCard} color="white" bg="blue.300"><AddIcon />Add card</Button>
                            </ModalFooter>
                        </ModalContent>
                    </Modal>
                </Box>
            </>
        )
    }
}
export default CardDetails;