import React, { Component } from 'react';
import * as TrelloApi from '../../api';
import DeleteIcon from '@mui/icons-material/Delete';
import {
    Box,
    Input,
    Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverBody,
    PopoverArrow,
    PopoverCloseButton,
} from '@chakra-ui/react';
import './style.css';
class checkItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkItems: [],
            hasError: false,
            checkItemName: '',
        }
    }
    componentDidMount() {
        TrelloApi.getCheckItems(this.props.checkListId)
            .then(checkItems => {
                this.setState({

                    checkItems: checkItems,
                    hasError: false,

                })
            })
    }
    createCheckItem = () => {
        TrelloApi.createCheckItems(this.props.checkListId, this.state.checkItemName)
            .then((res) => {
                const newCheckItems = [...this.state.checkItems];
                newCheckItems.push(res);
                this.setState({ checkItems: newCheckItems })
            })
    }
    handleDelete = (checkListId, checkItemId) => {
        TrelloApi.deleteCheckItems(checkListId, checkItemId)
            .then((res) => {
                const newChekItems = this.state.checkItems.filter((checkItem) => {
                    return checkItem.id !== checkListId && checkItem.id !== checkItemId;
                })
                this.setState({ checkItems: newChekItems });
            })
    }

    render() {
        const { checkItems } = this.state;
        return (
            <>
                {checkItems.map((checkItem, index) => {
                    return <Box key={index} display="flex">
                        <Box ml={4} mr={3} fontSize={18}> <input type="checkbox" /> <label className="strikethrough">{checkItem.name}</label></Box>
                        <Box ml={8} onClick={() => { this.handleDelete(this.props.checkListId, checkItem.id) }} cursor="pointer"><DeleteIcon /></Box>

                    </Box>
                })}
                <Popover placement="bottom" ml="8rem">
                    <PopoverTrigger>
                        <Button ml={8}>Add an item</Button>
                    </PopoverTrigger>
                    <PopoverContent ml="8rem">
                        <PopoverArrow />
                        <PopoverBody><Input onChange={e => { this.setState({ checkItemName: e.target.value }) }} placeholder="Add an item..." /></PopoverBody>
                        <Box >
                            <PopoverBody><Button ml="5rem" onClick={this.createCheckItem} bg="#026AA7" color="white">Add</Button></PopoverBody>
                            <PopoverCloseButton mt="4.2rem" mr="5rem" fontSize={18} />
                        </Box>
                    </PopoverContent>
                </Popover>

            </>
        )
    }
}


export function Hello() {
    return 'Hello'
}

export default checkItem;