import React, { Component } from 'react';
import * as TrelloApi from '../../api';
import {
    Box, Text,
    Input,
    Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverBody,
    PopoverArrow,
    PopoverCloseButton,
} from '@chakra-ui/react';
import CheckItem from '../CheckItem/CheckItem';

import CheckBoxOutlinedIcon from '@mui/icons-material/CheckBoxOutlined';
class CheckList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkList: [],
            hasError: false,
            isOpen: false,
            checkListName: '',
        };
    }

    handleOpen = () => {
        this.setState({ isOpen: true });
    }

    handClose = () => {
        this.setState({ isOpen: false });
    }

    handleDelete = (id) => {
        TrelloApi.deleteCheckList(id)
            .then((res) => {
                const newLists = this.state.checkList.filter((checkList) => {
                    return checkList.id !== id;
                })
                this.setState({ checkList: newLists });
            })
    }
    componentDidMount() {
        TrelloApi.getChecklist(this.props.cardId)
            .then(checkList => {
                this.setState({
                    checkList: checkList,
                    hasError: false,

                });
            }).catch((err) => {
                this.setState({
                    checkList: [],
                    hasError: "Failed to load checklist",
                });
            })

    }
    createCheckList = () => {
        TrelloApi.createCheckList(this.props.cardId, this.state.checkListName)
            .then((res) => {
                const newCheckLists = [...this.state.checkList];
                newCheckLists.push(res);
                this.setState({ checkList: newCheckLists, checkListName: '' })
            })
    }

    render() {
        const { checkList } = this.state;
        return (
            <> <Box mb={3} ml="18rem" >
                <Text>Add to card</Text>
                <Popover placement="bottom" ml="15rem">
                    <PopoverTrigger>
                        <Button w={165}><Box mr={3} ml={-1} mb={2}><CheckBoxOutlinedIcon /></Box>Checklist</Button>

                    </PopoverTrigger>
                    <PopoverContent w="20rem" h="15rem" ml="5rem">
                        <PopoverArrow />
                        <PopoverBody textAlign="center" fontWeight={700}>
                            Add checklist
                        </PopoverBody>
                        <PopoverBody>Title</PopoverBody>
                        <PopoverBody mt={-3}><Input onChange={event => this.setState({ checkListName: event.target.value })} /></PopoverBody>
                        <PopoverBody><Button bg="blue.300" w={100} color="white" onClick={this.createCheckList}>Add</Button></PopoverBody>
                        <Box >
                            <PopoverCloseButton fontSize={13} mt={2} />
                        </Box>
                    </PopoverContent>
                </Popover>
            </Box>
                <hr />
                {
                    checkList.map((checkList, index) => {
                        return (

                            <Box key={index} mt={2}>
                                <Box display="flex" p={2} justifyContent="space-between">   <CheckBoxOutlinedIcon /><Text ml={-60} fontWeight={700} fontSize={18}>{checkList.name}</Text>
                                    <Button onClick={() => { this.handleDelete(checkList.id) }}>Delete</Button>
                                </Box>
                                <CheckItem checkListId={checkList.id} />

                            </Box>
                        )

                    })

                }
            </>

        )
    }
}

export default CheckList;