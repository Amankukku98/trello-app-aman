import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as TrelloApi from '../../api';
import {
    Box, Heading, Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
} from '@chakra-ui/react';

class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boards: [],
            isLoading: true,
            hasError: false,
            boardName: '',
            isOpen: false,


        }
    }


    createBoard = () => {

        TrelloApi.createBoards(this.state.boardName)
            .then((res) => {
                const newBoards = [...this.state.boards];
                newBoards.push(res);
                this.setState({
                    boards: newBoards,
                    isOpen: false,
                })
            })
    }
    handleOpen = () => {
        this.setState({ isOpen: true });
    }
    handClose = () => {
        this.setState({ isOpen: false });
    }


    componentDidMount() {
        TrelloApi.getAllBoards()
            .then(boards => {
                this.setState({
                    boards: boards,
                    isLoading: false,
                    hasError: false,
                });
            })
            .catch((err) => {
                this.setState({
                    boards: [],
                    isLoading: false,
                    hasError: "Failed to load Boards",
                });
            })

    }


    render() {

        const { boards } = this.state;


        if (this.state.hasError) {
            return (
                <div>
                    <h1>{this.state.hasError}</h1>
                </div>
            )
        }

        if (this.state.isLoading) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            );
        }

        return (

            <Box display="flex" flexWrap="wrap">

                {
                    boards.map((board, index) => {
                        return (
                            <Link to={`/${board.id}`} key={index}>
                                <Box display="flex" flexWrap="wrap" >
                                    <Box onClick={this.handleBoard} backgroundImage="url('https://trello-backgrounds.s3.amazonaws.com/SharedBackground/original/49f33090455fdd208c1693adb9ecac7f/photo-1664737061963-862d6a174a3b')"
                                        backgroundPosition="center"
                                        backgroundRepeat="no-repeat"
                                        backgroundSize={300}
                                        h="10rem" w="20rem" ml="2rem" mt="5rem" color="white" textAlign="center" border="1px">
                                        <Heading as="h1">{board.name}</Heading>
                                    </Box >
                                </Box>
                            </Link>

                        )
                    })
                }
                <Box mt="5rem" ml="2rem">
                    <>
                        <Box bg="gray.200" w="20rem" h="10rem" textAlign="center" onClick={this.handleOpen}>
                            <Button bg="gray.200" mt={14} >Create new board</Button>
                        </Box>

                        <Modal isOpen={this.state.isOpen}>
                            <ModalOverlay />
                            <ModalContent mt="10rem">
                                <ModalHeader>Create a board</ModalHeader>
                                <ModalBody>
                                    <Input placeholder='Enter board title' onChange={e => { this.setState({ boardName: e.target.value }) }} ></Input>
                                </ModalBody>
                                <ModalFooter>
                                    <Button colorScheme='blue' mr={3} onClick={this.handClose}>
                                        Close
                                    </Button>
                                    <Button variant='ghost' onClick={this.createBoard} color="white" bg="blue.300">Create</Button>
                                </ModalFooter>
                            </ModalContent>
                        </Modal>
                    </>



                </Box>

            </Box >
        )
    }
}
export default Board;
