import React, { Component } from 'react';
import * as TrelloApi from '../../api';
import CardDetails from '../CardDetails/CardDetails';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import {
    Box, Heading, Button, Flex,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalFooter,
    ModalBody,
} from '@chakra-ui/react';


class ListDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            hasError: false,
            listName: '',
            cardDetail: false,
            isOpen: false,

        }
    }

    createList = () => {
        const { id } = this.props.match.params;
        TrelloApi.createList(id, this.state.listName)
            .then((res) => {
                const newLists = [...this.state.list];
                newLists.push(res);
                this.setState({
                    list: newLists,
                    isOpen: false,
                })
            })
    }
    handleOpen = () => {
        this.setState({ isOpen: true });
    }
    handClose = () => {
        this.setState({ isOpen: false });
    }

    handleDeleteList = (id) => {
        TrelloApi.deleteList(id, true)
            .then((res) => {
                const newLists = this.state.list.filter((list) => {
                    return list.id !== id;
                })
                this.setState({ list: newLists });
            })
    }


    componentDidMount() {
        const { id } = this.props.match.params;
        TrelloApi.getList(id)
            .then(list => {
                this.setState({
                    list: list,
                });
            }).catch((err) => {
                this.setState({
                    list: [],
                    hasError: "Failed to load lists",
                });
            })
    }
    render() {
        const { list, cardName } = this.state;
        return (
            <Box backgroundImage="url('https://mixkit.imgix.net/art/85/85-original.png-1000h.png')"
                h="100vh"
                w="100%"
                bgRepeat="no-repeat"
                backgroundPosition="center"
                backgroundSize="cover">

                <Flex wrap="wrap">
                    {
                        list.map((list, index) => {
                            return (
                                <Box>
                                    <Box bg="gray.300" w="300px" m={2} key={index} mt={5} p={1} borderRadius={5}>
                                        <Box display="flex" justifyContent="space-between">
                                            <Heading as="h6" fontSize={28} ml={2}>{list.name}</Heading><Box color="gray.700" mr={3}><DeleteIcon onClick={(id) => { this.handleDeleteList(list.id) }} cursor="pointer" /></Box>
                                        </Box>
                                        <Box>
                                            <CardDetails newCard={cardName} listId={list.id} />

                                        </Box>
                                    </Box>
                                </Box>
                            )

                        })
                    }

                    <>
                        <Box
                            h="3rem" bg="skyblue" w="300px" ml="2rem" mt={10} color="white" textAlign="center" border="1px">
                            <Button fontSize={20} mt={1} p="1rem" bg="skyblue" w="18.5rem" color="white" onClick={this.handleOpen}>  <AddIcon />Add another list</Button>
                        </Box >
                        <Modal isOpen={this.state.isOpen}>
                            <ModalOverlay />
                            <ModalContent mt="11rem" ml="22rem">
                                <ModalBody>
                                    <Input type="text" onChange={e => { this.setState({ listName: e.target.value }) }} placeholder="Enter list title..." />
                                </ModalBody>
                                <ModalFooter>
                                    <Button colorScheme='blue' mr={3} onClick={this.handClose}>
                                        Close
                                    </Button>
                                    <Button variant='ghost' onClick={this.createList} bg="#026AA7" color="white">Add list</Button>
                                </ModalFooter>
                            </ModalContent>
                        </Modal>

                    </>
                </Flex>
            </Box>
        )
    }
}
export default ListDetails;