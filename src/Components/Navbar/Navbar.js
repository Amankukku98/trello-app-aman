import React, { Component } from 'react';
import { Box, Flex, Heading, Button, Input } from '@chakra-ui/react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import SearchIcon from '@mui/icons-material/Search';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import AppsIcon from '@mui/icons-material/Apps';
class Navbar extends Component {
    render() {
        return (
            <Box>
                <Flex bg="#026AA7" alignItems="center" w="100%" mt={-8} mb={0}>

                    <Heading color="white" ml={5} display="flex" mt={12}><AppsIcon />

                        <Box mt={1} ml={1} boxSize={20}><img src='https://a.trellocdn.com/prgb/dist/images/header-logo-spirit-loading.87e1af770a49ce8e84e3.gif' alt="" /></Box></Heading>


                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Workspaces</p><ExpandMoreIcon /></Button>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Recent</p><ExpandMoreIcon /></Button>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Starred</p><ExpandMoreIcon /></Button>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Templates</p><ExpandMoreIcon /></Button>
                    <Button colorScheme='blue' fontSize={18} h={35} mt={0} bg="#0000004d" ml={10} p="1rem 2rem" border="none" color="white">Create</Button>
                    <Box display="flex" ml="55rem">
                        <Box color="black" bg="white" borderRadius={10} display="flex" alignItems="center" mt={2}>
                            <SearchIcon />
                            <Input type="text" placeholder='Search' border="none" variant='styled' color="red" w={200} />
                        </Box>
                        <Box bg="#026AA7" border="none" color="white" mt={18} ml={15}>
                            <InfoOutlinedIcon />
                        </Box>
                        <Box bg="#026AA7" border="none" color="white" mt={18} ml={15}>
                            <NotificationsNoneOutlinedIcon />
                        </Box>
                        <Box bg="#f59927cc" borderRadius={100} w={45} h={45} textAlign="center" mt={2} ml={18} p={2.5} mr={5}><p>AK</p></Box>
                    </Box>
                </Flex>
            </Box>
        )
    }
}
export default Navbar;